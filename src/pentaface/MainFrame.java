package pentaface;

public class MainFrame extends javax.swing.JFrame {
    public MainFrame() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pentaFace1 = new pentaface.PentaFace();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setFont(new java.awt.Font("Berlin Sans FB", 1, 48)); // NOI18N

        pentaFace1.setMargin(new java.awt.Insets(20, 20, 20, 20));
        pentaFace1.setStrokeWidth(7);
        getContentPane().add(pentaFace1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private pentaface.PentaFace pentaFace1;
    // End of variables declaration//GEN-END:variables
}
