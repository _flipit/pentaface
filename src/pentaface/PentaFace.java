package pentaface;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;

public class PentaFace extends JComponent {
    private static final Logger LOG
            = Logger.getLogger(PentaFace.class.getName());

    private static final double DEG36 = deg2rad(36);
    private static final double COS36 = Math.cos(DEG36);
    private static final double SIN36 = Math.sin(DEG36);
    private static final double DEG72 = deg2rad(72);
    private static final double SIN72 = Math.sin(DEG72);
    private static final double COS72 = Math.cos(DEG72);

    private final Insets margin = new Insets(4, 4, 4, 4);
    private int strokeWidth = 3;
    private Color strokeColor = Color.BLACK;
    private Color pentaColor = Color.WHITE;
    private String text = "\uf4ce";
    private int textSize = 48;
    private Font textFont;

    public PentaFace() {
        setPreferredSize(new Dimension(100, 100));
    }

    public Insets getMargin() {
        return new Insets(margin.top, margin.left, margin.bottom, margin.right);
    }

    public void setMargin(Insets margin) {
        this.margin.set(margin.top, margin.left, margin.bottom, margin.right);
        repaint();
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
        repaint();
    }

    public Color getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(Color strokeColor) {
        this.strokeColor = strokeColor;
        repaint();
    }

    public Color getPentaColor() {
        return pentaColor;
    }

    public void setPentaColor(Color pentaColor) {
        this.pentaColor = pentaColor;
        repaint();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        repaint();
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int fontSize) {
        this.textSize = fontSize;
        textFont = null;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        Dimension dim = getSize();
        int hi = dim.height-(margin.top+margin.bottom);
        int wi = dim.width-(margin.left+margin.right);
        double rh = hi/(COS36+1);
        double rw = wi/(2*SIN72);
        double r = Math.min(rh, rw);
        int xm = margin.left+wi/2;
        int ym = margin.top+(hi-(int)(r*(1+COS36)))/2;
        int[] xp = new int[] {
            xm+(int)(r*SIN36),
            xm+(int)(r*SIN72),
            xm,
            xm-(int)(r*SIN72),
            xm-(int)(r*SIN36),
        };
        int[] yp = new int[] {
            ym,
            ym+(int)(r*(COS36+COS72)),
            ym+(int)(r*(COS36+1)),
            ym+(int)(r*(COS36+COS72)),
            ym,
        };
        Shape penta = new Polygon(xp, yp, 5);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        Color savedColor = g2d.getColor();
        try {
            g2d.setColor(pentaColor);
            g2d.fill(penta);
        } finally {
            g2d.setColor(savedColor);
        }
        savedColor = g2d.getColor();
        try {
            g2d.setColor(strokeColor);
            Stroke savedStroke = g2d.getStroke();
            try {
                g2d.setStroke(new BasicStroke(strokeWidth,
                        BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
                g2d.draw(penta);
            } finally {
                g2d.setStroke(savedStroke);
            }
        } finally {
            g2d.setColor(savedColor);
        }
        if (textFont == null) {
            textFont = loadFont("/fa-solid-900.ttf", textSize);
        }
        g2d.setFont(textFont);
        FontMetrics fm = g2d.getFontMetrics(textFont);
        int x = xm-fm.stringWidth(text)/2;
        int y = ym+(int)(r*COS36)
                -(fm.getAscent()+fm.getDescent())/2+fm.getAscent();
        g2d.drawString(text, x, y);
    }

    private static double deg2rad(double angle) {
        return angle*2*Math.PI/360;
    }

    private static Font loadFont(String name, int size) {
        try (InputStream in = PentaFace.class.getResourceAsStream(name)) {
            Font font = Font.createFont(Font.TRUETYPE_FONT, in);
            return font.deriveFont(Font.PLAIN, size);
        } catch (IOException|FontFormatException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex.getMessage());
        }
    }
}
